<?php

 /**
  * hook_views_data -- define our views
  */
 function og_node_approval_views_data()
 {
	$atype = variable_get('approval_type',2);

	$data['og_node_approval']['table']['group'] = t('Node');

	$data['og_node_approval']['table']['join'] = array(
		'node' => array(
			'left_field' => 'nid',
			'field' => 'nid',
		),
	);

	$data['og_node_approval']['status'] = array(
		'title' => t('Node Approval: User Status'),
		'help' => t('The integer value for a particular user approval of this node.'),
		'field' => array(
			'handler' => 'views_handler_field_numeric',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
	);


	$data['og_node_approval']['uid'] = array(
		'title' => t('Node Approval: User Id'),
		'help' => t('The user id associated with the node approval and node'),
		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'relationship' => array(
			'handler' => 'views_handler_relationship',
			'base' => 'users',
			'relationship field' => 'uid',
			'label' => t('Node Approval: Users'),
		),
	);

    if ($atype == 2)
		_og_node_approval_views_add_node_status($data);
	else if ($atype == 0)
		_og_node_approval_views_add_status_handler($data);

	return $data;

 } // function og_node_approval_views_data


 /**
  * _og_node_approval_views_add_node_status -- only add this to views if we're using node_status table
  */
 function _og_node_approval_views_add_node_status(&$data)
 {
	$data['node_status']['table']['group'] = t('Node');
	$data['node_status']['table']['join'] = array(
		'node' => array(
			'left_field' => 'nid',
			'field' => 'nid',
		),
	);

	$data['node_status']['status'] = array(
		'title' => t('Node Approval: Status'),
		'help' => t('The integer value for a particular node approval status.'),
		'field' => array(
			'handler' => 'views_handler_field_numeric',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
	);
 } // function _og_node_approval_views_add_node_status


 function _og_node_approval_views_add_status_handler(&$data)
 {

 } // fuction _og_node_approval_views_add_status_handler
